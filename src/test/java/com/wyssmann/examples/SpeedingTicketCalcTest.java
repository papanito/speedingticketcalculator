package com.wyssmann.examples;

import org.junit.Test;

import static org.junit.Assert.*;

public class SpeedingTicketCalcTest {

    @Test
    public void isTicketTest() {
        SpeedingTicketCalc calc = new SpeedingTicketCalc();
        assertTrue(calc.isTicket(60,50));
    }

}
